import numpy as np
import pandas as pd


class SubmissionError(Exception):
    pass


def get_history(dataset: pd.DataFrame) -> pd.DataFrame:
    """
    Возвращает таблицу с историческими покупками (без кластеров-дубликатов для одного пользователя)
    Args:
        dataset:

    Returns:

    """
    assert len({"id", "cluster_id"}.intersection(dataset.columns)) == 2, dataset.columns

    return dataset[["id", "cluster_id"]].drop_duplicates()


def get_cluster_weights(dataset: pd.DataFrame) -> pd.DataFrame:
    """
    Рассчитывает веса кластеров на основе количества покупок продуктов из кластеров
    Args:
        dataset: данные о исторических покупках
get_cluster_weights
    Returns:

    """
    assert "cluster_id" in dataset.columns, dataset.columns

    cluster_popularity = dataset["cluster_id"].value_counts().sort_values(ascending=True).reset_index()
    cluster_popularity.columns = ["cluster_id", "cnt"]
    cluster_popularity["rank"] = cluster_popularity["cnt"].rank(method="dense") + 1
    cluster_popularity["w"] = 1 / np.log10(cluster_popularity["rank"])

    return cluster_popularity[["cluster_id", "w"]]


def competition_metric_low_memory(
    submission: pd.DataFrame,
    test: pd.DataFrame,
    cluster_weights: pd.DataFrame,
    history: pd.DataFrame,
    k: int = 20
) -> float:

    
    if len(set(submission["id"].unique()) ^ set(test["id"].unique())) > 0:
        raise SubmissionError("Length of disjunctive union of sets of submission and test ids greater than zero")
    
    submission = submission.set_index('id')['target'].to_dict()
    test = test.set_index('id')['target'].to_dict()
    
    n = len(submission)
    max_w = cluster_weights['w'].max()
    
    cluster_weights = cluster_weights.set_index('cluster_id')['w'].to_dict()
    history = history.groupby('id')['cluster_id'].apply(set).to_dict()
    
    recall_score = 0
    serendipity_score = 0

    for _id in submission:
        actual = test.get(_id, -1)
        if actual == -1:
            raise SubmissionError(f"id={_id} not in test set")

        hist = history.get(_id, -1)
        if hist == -1:
            raise SubmissionError(f"id={_id} not in history set")
        
        recs = [int(item) for item in submission[_id].split(';')]
        if len(recs) != len(set(recs)):
            raise SubmissionError("Duplicate ids in submission")
        
        if len(recs) > k:
            raise SubmissionError(f"Length of the recommendation list must be less than or equal to {k}")
        
        actual = set([int(item) for item in actual.split(';')])
        
        recall_score += sum(
            [
                cluster_weights.get(item, max_w)
                for item in recs 
                if item in actual
            ]
        ) / min(k, len(actual))
        serendipity_score += sum(
            [
                cluster_weights.get(item, max_w)
                for item in recs 
                if item in actual
                and item not in hist
            ]
        ) / min(k, len(actual))
    
    return 0.25 * recall_score / n + 0.75 * serendipity_score / n
