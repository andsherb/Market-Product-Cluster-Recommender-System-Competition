# About the competition
The food delivery industry (e-grocery) is now actively developing, and companies in this area are technological giants building a complex digital product and operating with a huge amount of data. SberMarket is the leader of e-grocery in Russia. Our mission is to save our customers time and money for something more important.

More than 100,000 families across Russia order from SberMarket every day, and it is important for us to make their interaction with the service as comfortable as possible. Sbermarket provides customers with personalized recommendations that help them remember important things, save money, or try something new.

The competition metric takes into account the nuances of applying the model to improve the user experience.

# Task description
In this competition, you have to predict what purchases a user will make in the next month.

SberMarket has more than 2 million product items, so we have grouped products into “clusters”.

Each cluster combines similar products according to their description. The clusters were built using ML, through product embeddings.

In order to be able to study the relationship of clusters, we will give you their "centers" and basic stats. characteristics.

In the test data, you will see the id of the clusters and you will predict exactly them.

# Data for training
train.parquet - historical data on user purchases
clusters.parquet - information about clusters
centroids.parquet - information about cluster centers
In historical user purchase data (June 1, 2021 to October 1, 2021), each row corresponds to one item purchased.

# Quality metric
The metric of this competition is the weighted sum of Recall@k and Serendipity@k (K = 20):

score = 0.25Recall@k + 0.75Serendipity@k

For the calculation, you can use the competition_metric function from the metric.py file, as well as the ready-made history.parquet and cluster_weights.parquet files. They are obtained using functions from metric.py on the training set (train.parquet).

Recall@k is one of the classic metrics for evaluating recommender systems, defined as the ratio of the number of relevant (positive) examples in examples with the highest rating k to the total number of relevant examples for recommender issuance.
